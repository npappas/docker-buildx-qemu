FROM debian
# Install Docker and qemu
# TODO Use docker stable once it properly supports buildx
RUN apt-get update && apt-get install -y \
	apt-transport-https \
	build-essential \
	ca-certificates \
	curl \
	gnupg2 \
	software-properties-common && \
	curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add - && \
	add-apt-repository "deb https://download.docker.com/linux/debian $(lsb_release -cs) stable" && \
	apt-get update && apt-get install -y \
	docker-ce-cli \
	binfmt-support \
	qemu-user-static \
	wget

# Install buildx plugin
RUN mkdir -p ~/.docker/cli-plugins && \
	ARCH=`dpkg --print-architecture` && echo Running on $ARCH && curl -s https://api.github.com/repos/docker/buildx/releases/latest | \
	grep "browser_download_url.*linux-$ARCH" | cut -d : -f 2,3 | tr -d \" | \
	xargs curl -L -o ~/.docker/cli-plugins/docker-buildx && \
	chmod a+x ~/.docker/cli-plugins/docker-buildx

# Write version file
RUN printf "$(docker --version | perl -pe 's/^.*\s(\d+\.\d+\.\d+.*),.*$/$1/')_$(docker buildx version | perl -pe 's/^.*v?(\d+\.\d+\.\d+).*$/$1/')" > /version && \
	cat /version

ENV RUSTUP_HOME=/opt/rustup
ENV CARGO_HOME=/opt/cargo
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y
ENV PATH="${PATH}:/opt/cargo/bin"

# Specify the preinstalled toolchain (the tag should match this)
ENV RUST_TOOLCHAIN="stable"

# Install toolchain
RUN rustup toolchain install "${RUST_TOOLCHAIN}"
RUN rustup default "${RUST_TOOLCHAIN}"

# Install clippy and rustfmt
RUN rustup component add clippy
RUN rustup component add rustfmt

# Install cross (and a docker client as it will be needed)
RUN curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
RUN apt-get install -y software-properties-common apt-transport-https ca-certificates curl gnupg2
RUN add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian stretch stable"
RUN apt-get update
RUN apt-get install -y docker-ce docker-ce-cli containerd.io
RUN cargo install --git="https://github.com/rust-embedded/cross.git" --branch="master" cross

# Modify cross tool to be able to access rustup and cargo from spawned container
RUN mv /opt/cargo/bin/cross /opt/cargo/bin/real-cross
COPY cross /opt/cargo/bin/cross
RUN chmod +x /opt/cargo/bin/cross

ENV HELM_LATEST_VERSION="v3.1.2"

RUN wget -q https://get.helm.sh/helm-${HELM_LATEST_VERSION}-linux-amd64.tar.gz \
	&& tar -xf helm-${HELM_LATEST_VERSION}-linux-amd64.tar.gz \
	&& mv linux-amd64/helm /usr/local/bin \
	&& rm -f /helm-${HELM_LATEST_VERSION}-linux-amd64.tar.gz
